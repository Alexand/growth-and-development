## What's this issue about?

The purpose of this issue is to track my weekly planning, much like we do with our Slack stand up, but with some more advantages, like:

- Keeping records that won't be purged after 3 months. 
- Better support for commenting and assessment.

What I hope to achieve with this issue:

- Have a better view of my own weekly "say/do" ratio, so I can access whether I'm delivering much/little, and whether I'm promising much/little.
- Be able to better gauge my own Retro. That is, what have been my drawbacks during these weeks, and what have made me succeed during these weeks, so I that can work on being more productive by repeating the good and fixing the bad.
- Provide better transparency to my managers and direct teammates over what I'm working on.

## Templates

### Plan for YYYY-MM-DD to YYYY-MM-DD

#### MRs

- [ ] 

#### Issues

- [ ] 

#### Reviews

- [ ] 

#### Others

- [ ] 

#### Risks

- [ ] Consider and document week risks and plan of actions to mitgate them.
  - Social distractions?
  - Trips?
  - External Reviews?
  - Technical Difficulties?
  - Private concerns?

---

### Retro

#### MRs

- [ ] 

#### Issues

- [ ] 

#### Reviews

- [ ] 

#### Others

- [ ] Consider updating the CREDIT tracker with these week’s achievement
