# Growth and Development

This is a personal project to help me:

- Track my personal growth & development within GitLab.
- Track my personal achievements.
- Track my weekly work progress.
- Help me assess my strengths as well as my weaknesses and improvement opportunities.

# Tracking Issues and Documents

- [Weekly progress](https://gitlab.com/Alexand/growth-and-development/-/issues/1)
- [CREDIT Tracker](https://docs.google.com/document/d/16qoQWsorEECDjSKt4TjgE-oLmuoDmLc6samtWjU6kLA/edit)
- [Promotion Doc](https://docs.google.com/document/d/1F8ibDXV6ovpLC_yWbnqAN3PNGy62sSeeDxy3uHQt17o/edit#heading=h.5ahg1o4v53q7)
